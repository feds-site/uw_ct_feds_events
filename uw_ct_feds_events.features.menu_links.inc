<?php
/**
 * @file
 * uw_ct_feds_events.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function uw_ct_feds_events_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-site-manager-vocabularies_feds-event-ages:admin/structure/taxonomy/event_ages.
  $menu_links['menu-site-manager-vocabularies_feds-event-ages:admin/structure/taxonomy/event_ages'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/event_ages',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'Feds Event Ages',
    'options' => array(
      'attributes' => array(
        'title' => 'Event category ages such as "+19" or "all ages"',
      ),
      'identifier' => 'menu-site-manager-vocabularies_feds-event-ages:admin/structure/taxonomy/event_ages',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-site-manager-vocabularies_feds-event-types:admin/structure/taxonomy/feds_event_types.
  $menu_links['menu-site-manager-vocabularies_feds-event-types:admin/structure/taxonomy/feds_event_types'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/feds_event_types',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'Feds event types',
    'options' => array(
      'attributes' => array(
        'title' => 'Categories for filtering event types.',
      ),
      'identifier' => 'menu-site-manager-vocabularies_feds-event-types:admin/structure/taxonomy/feds_event_types',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Feds Event Ages');
  t('Feds event types');

  return $menu_links;
}
