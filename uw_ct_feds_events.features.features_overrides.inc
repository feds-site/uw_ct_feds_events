<?php

/**
 * @file
 * uw_ct_feds_events.features.features_overrides.inc
 */

/**
 * Implements hook_features_override_default_overrides().
 */
function uw_ct_feds_events_features_override_default_overrides() {
  // This code is only used for UI in features. Export alter hooks do the magic.
  $overrides = array();

  // Exported overrides for: user_permission.
  $overrides["user_permission.enter feds_event_listing_header revision log entry.roles|WCMS support"] = 'WCMS support';
  $overrides["user_permission.enter feds_event_listing_header revision log entry.roles|administrator"] = 'administrator';
  $overrides["user_permission.enter feds_event_listing_header revision log entry.roles|content editor"] = 'content editor';
  $overrides["user_permission.enter feds_event_listing_header revision log entry.roles|site manager"] = 'site manager';
  $overrides["user_permission.override feds_event_listing_header authored by option.roles|WCMS support"] = 'WCMS support';
  $overrides["user_permission.override feds_event_listing_header authored by option.roles|administrator"] = 'administrator';
  $overrides["user_permission.override feds_event_listing_header authored by option.roles|site manager"] = 'site manager';
  $overrides["user_permission.override feds_event_listing_header authored on option.roles|WCMS support"] = 'WCMS support';
  $overrides["user_permission.override feds_event_listing_header authored on option.roles|administrator"] = 'administrator';
  $overrides["user_permission.override feds_event_listing_header authored on option.roles|site manager"] = 'site manager';
  $overrides["user_permission.override feds_event_listing_header promote to front page option.roles|administrator"] = 'administrator';
  $overrides["user_permission.override feds_event_listing_header promote to front page option.roles|site manager"] = 'site manager';
  $overrides["user_permission.override feds_event_listing_header published option.roles|WCMS support"] = 'WCMS support';
  $overrides["user_permission.override feds_event_listing_header published option.roles|administrator"] = 'administrator';
  $overrides["user_permission.override feds_event_listing_header published option.roles|content editor"] = 'content editor';
  $overrides["user_permission.override feds_event_listing_header published option.roles|site manager"] = 'site manager';
  $overrides["user_permission.override feds_event_listing_header revision option.roles|WCMS support"] = 'WCMS support';
  $overrides["user_permission.override feds_event_listing_header revision option.roles|administrator"] = 'administrator';
  $overrides["user_permission.override feds_event_listing_header revision option.roles|content editor"] = 'content editor';
  $overrides["user_permission.override feds_event_listing_header revision option.roles|site manager"] = 'site manager';
  $overrides["user_permission.override feds_event_listing_header sticky option.roles|administrator"] = 'administrator';
  $overrides["user_permission.override feds_event_listing_header sticky option.roles|site manager"] = 'site manager';

  return $overrides;
}
