<?php
/**
 * @file
 * uw_ct_feds_events.features.uuid_term.inc
 */

/**
 * Implements hook_uuid_features_default_terms().
 */
function uw_ct_feds_events_uuid_features_default_terms() {
  $terms = array();

  $terms[] = array(
    'name' => 'On Campus - Satellite Campus',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 2,
    'uuid' => '20229821-e4f9-468c-a727-0a9560ff94ce',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'feds_event_location',
    'translations' => array(
      'original' => NULL,
      'data' => array(),
    ),
    'path' => array(
      'pathauto' => 0,
    ),
    'url_alias' => array(),
  );
  $terms[] = array(
    'name' => 'Cultural',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 3,
    'uuid' => '39db4a3b-a067-49ef-9bec-a81b0238348f',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'feds_event_types',
    'translations' => array(
      'original' => NULL,
      'data' => array(),
    ),
    'path' => array(
      'pathauto' => 0,
    ),
    'url_alias' => array(),
  );
  $terms[] = array(
    'name' => 'Feds Hosted Events',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 5,
    'uuid' => '4581eb6c-5de9-4afe-b282-c7e45966e386',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'feds_event_types',
    'translations' => array(
      'original' => NULL,
      'data' => array(),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
    'url_alias' => array(
      0 => array(
        'alias' => 'feds-event-types/feds-hosted-events',
        'language' => 'und',
      ),
    ),
  );
  $terms[] = array(
    'name' => 'Educational',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 4,
    'uuid' => '5dc033e1-4c42-4356-a53f-8a20d539db36',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'feds_event_types',
    'translations' => array(
      'original' => NULL,
      'data' => array(),
    ),
    'path' => array(
      'pathauto' => 0,
    ),
    'url_alias' => array(),
  );
  $terms[] = array(
    'name' => 'All Ages',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => '601fe54a-ed38-46d1-b5cd-208cbe34dfc6',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'event_ages',
    'translations' => array(
      'original' => NULL,
      'data' => array(),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
    'url_alias' => array(
      0 => array(
        'alias' => 'event-ages/all-ages',
        'language' => 'und',
      ),
    ),
  );
  $terms[] = array(
    'name' => 'On Campus - Waterloo (Main Campus)',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 1,
    'uuid' => '6f6258d4-fc43-4025-b55c-1ef4b0a68f00',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'feds_event_location',
    'translations' => array(
      'original' => NULL,
      'data' => array(),
    ),
    'path' => array(
      'pathauto' => 0,
    ),
    'url_alias' => array(),
  );
  $terms[] = array(
    'name' => 'Clubs & Societies',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 2,
    'uuid' => '7006c7de-f0d1-4c92-8701-5b511439df4f',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'feds_event_types',
    'translations' => array(
      'original' => NULL,
      'data' => array(),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
    'url_alias' => array(
      0 => array(
        'alias' => 'feds-event-types/clubs-societies',
        'language' => 'und',
      ),
    ),
  );
  $terms[] = array(
    'name' => '19+',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 1,
    'uuid' => '84e656fd-4274-4890-82dd-7669b3e7c4b8',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'event_ages',
    'translations' => array(
      'original' => NULL,
      'data' => array(),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
    'url_alias' => array(
      0 => array(
        'alias' => 'event-ages/19',
        'language' => 'und',
      ),
    ),
  );
  $terms[] = array(
    'name' => 'Wrap Up Week',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 10,
    'uuid' => '8fdc8a39-692b-455c-8a09-8a0f96e20f5f',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'feds_event_types',
    'translations' => array(
      'original' => NULL,
      'data' => array(),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
    'url_alias' => array(
      0 => array(
        'alias' => 'feds-event-types/wrap-week',
        'language' => 'und',
      ),
    ),
  );
  $terms[] = array(
    'name' => 'Orientation',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 7,
    'uuid' => '908e1981-006b-4553-9f78-b714bf1f5b1d',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'feds_event_types',
    'translations' => array(
      'original' => NULL,
      'data' => array(),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
    'url_alias' => array(
      0 => array(
        'alias' => 'feds-event-types/orientation',
        'language' => 'und',
      ),
    ),
  );
  $terms[] = array(
    'name' => 'High Energy',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 6,
    'uuid' => '95148c99-00c1-4fb1-9ac3-e8925a986482',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'feds_event_types',
    'translations' => array(
      'original' => NULL,
      'data' => array(),
    ),
    'path' => array(
      'pathauto' => 0,
    ),
    'url_alias' => array(),
  );
  $terms[] = array(
    'name' => 'Reoccurring',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 8,
    'uuid' => '9b138a81-1edb-4bbd-9b9b-a86d26cfdd36',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'feds_event_types',
    'translations' => array(
      'original' => NULL,
      'data' => array(),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
    'url_alias' => array(
      0 => array(
        'alias' => 'feds-event-types/reoccurring',
        'language' => 'und',
      ),
    ),
  );
  $terms[] = array(
    'name' => 'Welcome Week',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 9,
    'uuid' => 'a903649c-6221-4ae3-914f-429fad0791fc',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'feds_event_types',
    'translations' => array(
      'original' => NULL,
      'data' => array(),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
    'url_alias' => array(
      0 => array(
        'alias' => 'feds-event-types/welcome-week',
        'language' => 'und',
      ),
    ),
  );
  $terms[] = array(
    'name' => 'Off Campus',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 3,
    'uuid' => 'b7ab676c-e4ce-40d3-8d90-68316c985a0a',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'feds_event_location',
    'translations' => array(
      'original' => NULL,
      'data' => array(),
    ),
    'path' => array(
      'pathauto' => 0,
    ),
    'url_alias' => array(),
  );
  $terms[] = array(
    'name' => 'Student Life Centre (SLC)',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => 'c3eaf391-46db-4ead-89d7-3b70a9f2df39',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'feds_event_location',
    'translations' => array(
      'original' => NULL,
      'data' => array(),
    ),
    'path' => array(
      'pathauto' => 0,
    ),
    'url_alias' => array(),
  );
  $terms[] = array(
    'name' => 'Athletics',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 1,
    'uuid' => 'd2be33a6-ce18-4ca3-87d6-cd51af131eca',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'feds_event_types',
    'translations' => array(
      'original' => NULL,
      'data' => array(),
    ),
    'path' => array(
      'pathauto' => 0,
    ),
    'url_alias' => array(),
  );
  $terms[] = array(
    'name' => '- All -',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => 'fa510f6d-201a-4dd1-aa1b-3074e5bd563b',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'feds_event_types',
    'translations' => array(
      'original' => NULL,
      'data' => array(),
    ),
    'path' => array(
      'pathauto' => 0,
    ),
    'url_alias' => array(),
  );
  return $terms;
}
