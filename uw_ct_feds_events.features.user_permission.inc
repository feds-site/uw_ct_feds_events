<?php
/**
 * @file
 * uw_ct_feds_events.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_ct_feds_events_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create feds_events content'.
  $permissions['create feds_events content'] = array(
    'name' => 'create feds_events content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'define view for terms in feds_event_location'.
  $permissions['define view for terms in feds_event_location'] = array(
    'name' => 'define view for terms in feds_event_location',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'tvi',
  );

  // Exported permission: 'define view for terms in feds_event_types'.
  $permissions['define view for terms in feds_event_types'] = array(
    'name' => 'define view for terms in feds_event_types',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'tvi',
  );

  // Exported permission: 'define view for vocabulary feds_event_location'.
  $permissions['define view for vocabulary feds_event_location'] = array(
    'name' => 'define view for vocabulary feds_event_location',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'tvi',
  );

  // Exported permission: 'define view for vocabulary feds_event_types'.
  $permissions['define view for vocabulary feds_event_types'] = array(
    'name' => 'define view for vocabulary feds_event_types',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'tvi',
  );

  // Exported permission: 'delete any feds_events content'.
  $permissions['delete any feds_events content'] = array(
    'name' => 'delete any feds_events content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own feds_events content'.
  $permissions['delete own feds_events content'] = array(
    'name' => 'delete own feds_events content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete terms in feds_event_location'.
  $permissions['delete terms in feds_event_location'] = array(
    'name' => 'delete terms in feds_event_location',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'delete terms in feds_event_types'.
  $permissions['delete terms in feds_event_types'] = array(
    'name' => 'delete terms in feds_event_types',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit any feds_events content'.
  $permissions['edit any feds_events content'] = array(
    'name' => 'edit any feds_events content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own feds_events content'.
  $permissions['edit own feds_events content'] = array(
    'name' => 'edit own feds_events content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit terms in feds_event_location'.
  $permissions['edit terms in feds_event_location'] = array(
    'name' => 'edit terms in feds_event_location',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit terms in feds_event_types'.
  $permissions['edit terms in feds_event_types'] = array(
    'name' => 'edit terms in feds_event_types',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'enter feds_events revision log entry'.
  $permissions['enter feds_events revision log entry'] = array(
    'name' => 'enter feds_events revision log entry',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_events authored by option'.
  $permissions['override feds_events authored by option'] = array(
    'name' => 'override feds_events authored by option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_events authored on option'.
  $permissions['override feds_events authored on option'] = array(
    'name' => 'override feds_events authored on option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_events promote to front page option'.
  $permissions['override feds_events promote to front page option'] = array(
    'name' => 'override feds_events promote to front page option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_events published option'.
  $permissions['override feds_events published option'] = array(
    'name' => 'override feds_events published option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_events revision option'.
  $permissions['override feds_events revision option'] = array(
    'name' => 'override feds_events revision option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_events sticky option'.
  $permissions['override feds_events sticky option'] = array(
    'name' => 'override feds_events sticky option',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'search feds_events content'.
  $permissions['search feds_events content'] = array(
    'name' => 'search feds_events content',
    'roles' => array(
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'search_config',
  );

  return $permissions;
}
