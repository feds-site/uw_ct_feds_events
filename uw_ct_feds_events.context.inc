<?php
/**
 * @file
 * uw_ct_feds_events.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function uw_ct_feds_events_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'feds_homepage_events';
  $context->description = 'Displays event block on a site\'s front page.';
  $context->tag = 'Feds';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-feds_events-feds_hp_events' => array(
          'module' => 'views',
          'delta' => 'feds_events-feds_hp_events',
          'region' => 'feds_events',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Displays event block on a site\'s front page.');
  t('Feds');
  $export['feds_homepage_events'] = $context;

  return $export;
}
